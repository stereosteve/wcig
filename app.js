var colors = ['#186dee', '#d64430', '#ffb80a', '#05a35e']
var state = {}

$('body').on('click', '.letter', function(ev) {
  var tar = $(ev.target)
  var key = tar.attr('class')
  if (state[key] == undefined) state[key] = -1
  state[key]++
  tar.css('background-color', colors[ state[key] % colors.length ])
})
